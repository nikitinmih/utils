package logger

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
)

const (
	DebugLvl = iota
	InfoLvl
	WarningLvl
	ErrorLvl
)

type Log struct {
	infoLog    *log.Logger
	warningLog *log.Logger
	errorLog   *log.Logger
	debugLog   *log.Logger
	buf        bytes.Buffer
	bufMaxLen  int
	level      int
	path string
}

func New(logPath string) *Log {
	l := &Log{bufMaxLen: 500, level: DebugLvl, path: logPath}
	l.createLogDir()
	infoHandle := io.MultiWriter(os.Stdout, &l.buf)
	warningHandle := io.MultiWriter(os.Stdout, &l.buf)
	errorHandle := io.MultiWriter(os.Stderr, &l.buf)
	debugHandle := io.MultiWriter(os.Stdout, &l.buf)
	logFormat := log.Ldate | log.Ltime | log.Lshortfile
	l.infoLog = log.New(infoHandle, "INFO:    ", logFormat)
	l.warningLog = log.New(warningHandle, "WARNING: ", logFormat)
	l.errorLog = log.New(errorHandle, "ERROR:   ", logFormat)
	l.debugLog = log.New(debugHandle, "DEBUG:   ", logFormat)
	return l
}

func (l *Log) createLogDir() {
	logDir := filepath.Dir(l.path)
	if _, err := os.Stat(logDir); os.IsNotExist(err) {
		os.MkdirAll(logDir, os.ModePerm)
	}
}

func (l *Log) Flush() {
	f, err := os.OpenFile(l.path, os.O_CREATE|os.O_WRONLY|os.O_APPEND, os.ModeAppend)
	if err != nil {
		if err != nil {
			l.errorLog.Output(2, "Ошибка открытия лог-файла: "+err.Error())
		}
	}
	defer f.Close()
	if _, err = f.Write(l.buf.Bytes()); err != nil {
		l.errorLog.Output(2, "Ошибка записи лога в файл: "+err.Error())
	}
	l.buf.Reset()
}

func (l *Log) SetLevel(level int) {
	l.level = level
}

func (l *Log) autoFlush() {
	if l.buf.Len() > l.bufMaxLen {
		l.Flush()
	}
}

func (l *Log) Debugf(format string, v ...interface{}) {
	if l.level <= DebugLvl {
		l.debugLog.Output(2, fmt.Sprintf(format, v...))
		l.autoFlush()
	}
}

func (l *Log) Infof(format string, v ...interface{}) {
	if l.level <= InfoLvl {
		l.infoLog.Output(2, fmt.Sprintf(format, v...))
		l.autoFlush()
	}
}

func (l *Log) Warningf(format string, v ...interface{}) {
	if l.level <= WarningLvl {
		l.warningLog.Output(2, fmt.Sprintf(format, v...))
		l.autoFlush()
	}
}

func (l *Log) Errorf(format string, v ...interface{}) {
	if l.level <= ErrorLvl {
		l.errorLog.Output(2, fmt.Sprintf(format, v...))
		l.autoFlush()
	}
}

func (l *Log) Panicf(format string, v ...interface{}) {
	l.errorLog.Panicf(format, v)
}

func (l *Log) Fatalf(format string, v ...interface{}) {
	l.errorLog.Fatalf(format, v)
}
