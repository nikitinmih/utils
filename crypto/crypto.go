package crypto

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/hex"
	"errors"
	"io"
)

var (
	// Секретный ключ
	key = []byte("A7D481496AE6CF08697A117EC0CFFED3")
)

func Encrypt(plaintext string) (string, error) {
	plaintextBytes := []byte(plaintext)
	c, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	gcm, err := cipher.NewGCM(c)
	if err != nil {
		return "", err
	}

	nonce := make([]byte, gcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		return "", err
	}

	return hex.EncodeToString(gcm.Seal(nonce, nonce, plaintextBytes, nil)), nil
}

func Decrypt(ciphertext string) (string, error) {
	ciphertextByte, err := hex.DecodeString(ciphertext)
	if err != nil {
		return "", nil
	}
	c, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	gcm, err := cipher.NewGCM(c)
	if err != nil {
		return "", err
	}

	nonceSize := gcm.NonceSize()
	if len(ciphertextByte) < nonceSize {
		return "", errors.New("Ciphertext too short.")
	}

	nonce, ciphertextByte := ciphertextByte[:nonceSize], ciphertextByte[nonceSize:]
	decryptData, err := gcm.Open(nil, nonce, ciphertextByte, nil)
	if err != nil {
		return "", err
	}
	return string(decryptData), nil
}
